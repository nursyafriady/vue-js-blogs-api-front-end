import axios from 'axios'
import swal from 'sweetalert';

export default {
    namespaced : true,
    state: {
        blogs       : [],
        title       : '',
        description : '',
        id          : null,
        photo       : null,
        loading     : false,
        status      : 'CREATE',
        apiDomain   : 'http://demo-api-vue.sanbercloud.com'
    },
    
    mutations: {
        editBlog    : (state, payload) => {
            state.status = 'UPDATE'
            state.title = payload.title
            state.description = payload.description
            state.id = payload.id
        },
        editPhoto   : (state, payload) => {
            state.status = 'UPLOAD'
            state.title = payload.title
            state.description = payload.description
            state.id = payload.id
        },
        changePhoto : (state, payload) => {
            state.photo = payload.target.files[0]
        },
        updateTitle : (state, payload) => {
            state.title = payload
        },
        updateDesc  : (state, payload) => {
            state.description = payload
        },
        clearForm   : (state) => {
            state.status = 'CREATE'
            state.title = ''
            state.description = ''
            state.id = null
            state.photo = null
            state.loading = false
        }
    },

    actions: {
        deleteBlog  : (context, payload) => {
            let confirm = window.confirm('Apakah anda yakin menghapus blog ??')

            if (confirm == true) {
                const config = {
                    method  : 'post',
                    url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${payload.id}`,
                    params  : { _method : 'DELETE' }
                }
    
                axios(config)
                    .then( (response) => {
                        context.dispatch('getBlogs')
                        swal({
                            title   : response.data.message + " Deleting Blog",
                            button  : 'Close',
                            timer   : 3000
                        })
                    })
                    .catch( (error) => {
                        console.log(error)
                    })
            }
        },
        submitForm  : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('title', context.state.title)
            formData.append('description', context.state.description)

            const config = {
                method  : 'post',
                url     : 'http://demo-api-vue.sanbercloud.com/api/v2/blog',
                data    : formData
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Add New Blog",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        updateBlog  : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('title', context.state.title)
            formData.append('description', context.state.description)

            const config = {
                method  : 'post',
                url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${context.state.id}`,
                data    : formData,
                params  : { _method : 'PUT' }
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Updating Blog",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        uploadPhoto : (context) => {
            context.state.loading = true

            let formData = new FormData()
            formData.append('photo', context.state.photo)

            const config = {
                method  : 'post',
                url     : `http://demo-api-vue.sanbercloud.com/api/v2/blog/${context.state.id}/upload-photo`,
                data    : formData,
            }

            axios(config)
                .then( (response) => {
                    context.commit('clearForm')
                    context.dispatch('getBlogs')
                    swal({
                        title   : response.data.message + " Uploading Photo",
                        button  : 'Close',
                        timer   : 3000
                    })
                })
                .catch ( (error) => {
                    context.commit('clearForm')
                    console.log(error)
                })
        },
        getBlogs    : (context) => {
            const config = {
                method: 'get',
                url : context.state.apiDomain + '/api/v2/blog/random/50'
            }

            axios(config)      
                .then( (response) => {
                    let { blogs } = response.data;
                    context.state.blogs = blogs;
                })            
                .catch( (error) => {
                    console.log(error);
                })
        },
    },
    
    getters : {
        blogs       : (state) => {
            return state.blogs
        },
        loading     : (state) => {
            return state.loading
        },
        status      : (state) => {
            return state.status
        },
        apiDomain   : (state) => {
            return state.apiDomain
        },
        title       : (state) => {
            return state.title
        },
        description : (state) => {
            return state.description
        },
    }
}