import axios from 'axios'

export default {
    namespaced : true,
    state: {
        authenticated: false,
        user: [],
        token : '',
    },

    getters : {
        check(state) {
           return state.authenticated
        },

        user(state) {
            return state.user
        },

        token(state) {
            return state.token
        }
    },
    
    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticated = value            
        },
        SET_USER(state, value) {
            state.user = value            
        },
        SET_TOKEN(state, value) {
            state.token = value            
        },
    },

    actions: {        
        async login({dispatch}, credentials) {
            await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/login', credentials)
            dispatch('me') 
            let response = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/login', credentials)
            // this.token = user.data.access_token; 
            // this.token = response; 
            // console.log(response);    
            const token = response.data.access_token;
            this.token = response.data.access_token
            // console.log(response.data.access_token)
            // console.log(this.token)
            localStorage.setItem("token", token);
            // console.log(localStorage.setItem("token", token))
        },

        async me({ commit }) {            
            try {
                // let { data } = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/me')
                let response = await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/me')
                commit('SET_AUTHENTICATED', true)
                commit('SET_USER', response.data)
                commit('SET_TOKEN', response.data.access_token)
                this.token = response.data.access_token
            } catch (e) {
                commit('SET_AUTHENTICATED', false)
                commit('SET_USER', [])
                commit('SET_TOKEN', '')
            }  
        },

        async signout({dispatch}) {
            await axios.post('http://demo-api-vue.sanbercloud.com/api/v2/auth/logout')
            dispatch('me')
        },
    },
}