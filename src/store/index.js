import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import blogs from './blogs'

Vue.use(Vuex)

export default new Vuex.Store({
  modules : {
    auth,
    blogs
  },
})
